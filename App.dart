import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());
//SafeArea
class HelloFlutterApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: SafeArea (
        child: Text("Hello Flutter"),
      ),
    );
  }
}